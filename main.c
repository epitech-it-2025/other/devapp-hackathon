#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[], char **env)
{
    if (argc != 2) {
        printf("We need the password");
        return 84;
    }

    if (argv == NULL || argv[1] == NULL)
        return 84;

    // We remove the hard password 
    // You Can hack me ;)
    if (strcmp(argv[1], getenv("SHELLPASSWORD")) == 0) {
        printf("OK");
        return 0;
    } else {
        printf("Bad Password");
        return 84;
    }
    return 0;
}
